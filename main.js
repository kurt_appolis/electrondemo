const electron = require('electron');
const url = require('url');
const path = require('path');

const { app, BrowserWindow, Menu, ipcMain } = electron;

// set env
process.env.NODE_ENV = 'production';


let mainWindow;
let addWindow;

// Listen for app to be ready
app.on('ready', function(){
    // Create a new windw
    mainWindow = new BrowserWindow({});

    // load html file into the window
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'mainwindow.html'),
        protocol: 'file',
        slashes: true
    }));

    // quit app when closed
    mainWindow.on('closed', function(){
        app.quit();
    });

    // Build menu from the template
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);

    // insert the menu
    Menu.setApplicationMenu(mainMenu);
}); 

// handle add window "create wind"
function createAddWindow(){
    // Create a new windw
    addWindow = new BrowserWindow({
        width: 300,
        height: 200,
        title: 'Add shopping list item'
    });

    // load html file into the window
    addWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'addwindow.html'),
        protocol: 'file',
        slashes: true
    }));

    // Garage collection handle
    addWindow.on('closed', function(){
        addWindow = null;
    })
}
// catch item add
ipcMain.on('item:add', function(e, item){
    mainWindow.webContents.send('item:add', item);
    addWindow.close();
});


// Create menu template
const mainMenuTemplate = [
    {
        label: 'File',
        submenu: [
            {
                label: 'Add Item',
                click(){
                    createAddWindow();
                }
            },
            {
                label: 'Clear items',
                click(){
                    mainWindow.webContents.send('item:clear');
                }
            },
            {
                label: 'Quit',
                accelerator: process.platform == "darwin" ? "Command+Q" : "Ctrl+Q",
                click(){
                    app.quit();
                }
            }
        ]
    }
 ];

 // If mac, add empty object to menu
 if(process.platform == "darwin"){
    // added to the beginning
    mainMenuTemplate.unshift({});
 }

 // Add dev tool if not in productions
 if(process.env.NODE_ENV !== 'production'){
    mainMenuTemplate.push({
        label: 'Developer Tools',
        submenu: [
        {
            label: 'Toggle DevTools',
            accelerator: process.platform == "darwin" ? "Command+I" : "Ctrl+I",
            click(item, focusedWindow){
                focusedWindow.toggleDevTools();
            }
        },
        {
            role: 'reload'
        }
        ]
    });
 }