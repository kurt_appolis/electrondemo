# README #

A demo version of the electron app.

### What is this repository for? ###

* Learning


### How do I get set up? ###

> npm install

> npm start

> npm run [package-mac|package-win|package-linux]


### Resources ###

* Tutorial: https://www.youtube.com/watch?v=kN1Czs0m1SU
* Blog article for publishing: https://www.christianengvall.se/electron-packager-tutorial/
* Main Electron doc: http://electronjs.org